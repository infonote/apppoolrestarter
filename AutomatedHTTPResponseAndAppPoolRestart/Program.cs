﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.Web.Administration;
using System.Diagnostics;

namespace AutomatedHTTPResponseAndAppPoolRestart
{
    class Program
    {
        public const int maxCount = 300; // for time out on app pool stop / start
        public static void Main()
        {
            var AppPoolName = ConfigurationManager.AppSettings["AppPoolName"];
            TestServerResponseAndRestart(AppPoolName);
        }
        /*
        private static void OnTimedEvent()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://ids.infonote.com/RadioService/RadioService/StreamData");

            CredentialCache cc = new CredentialCache();
            cc.Add(new Uri("http://ids.infonote.com/RadioService/RadioService/StreamData"), "Basic", new NetworkCredential("CreativeArts", "Cr3At1ve4rts"));

            request.Credentials = cc;

            Console.Write(DateTime.Now + " - Current Radio App status is ");
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                response.GetResponseStream();
                if (response != null || response.StatusCode == HttpStatusCode.OK)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write(HttpStatusCode.OK);
                    Console.ResetColor();
                    Console.WriteLine();

                    var TestEmail = ConfigurationManager.AppSettings["TestEmail"];
                    if (TestEmail == "true")
                    {
                        if (ConfigurationManager.AppSettings["SendEmail"] == "true")
                        {
                            var emailAddresses = ConfigurationManager.AppSettings["EmailList"] ?? "daniel.barrett@infonote.com";
                            var emailFrom = ConfigurationManager.AppSettings["EmailFrom"];
                            if (!string.IsNullOrWhiteSpace(emailAddresses))
                            {
                                string[] splitEmails = emailAddresses.Split(',');
                                foreach (var email in splitEmails)
                                {
                                    try
                                    {
                                        if (!string.IsNullOrWhiteSpace(email))
                                        {
                                            SmtpClient smtp = new SmtpClient();
                                            MailMessage Message = new MailMessage();
                                            Message.To.Add(email);
                                            Message.From = new MailAddress(emailFrom);
                                            Message.Subject = "Automated Message - Radio Apps Mail Client is working";
                                            Message.Body = "This email is testing that the radio apps mail client is working.<br /> <br /> To disable this message, change the TestEmail in the app.config to false.";
                                            Message.IsBodyHtml = true;

                                            smtp.Send(Message);
                                            Console.WriteLine("Sending Message..");
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine("Failure to send message Exception: " + ex.Message);
                                    }
                                }
                            }
                        }
                    }
                    response.Close();
                }
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Not " + HttpStatusCode.OK);
                Console.ResetColor();
                Console.WriteLine();

                if (ConfigurationManager.AppSettings["SendEmail"] == "true")
                {
                    var emailAddresses = ConfigurationManager.AppSettings["EmailList"] ?? "daniel.barrett@infonote.com";
                    var emailFrom = ConfigurationManager.AppSettings["EmailFrom"];
                    var EMailUser = ConfigurationManager.AppSettings["EMailUser"];
                    var EMailPassword = ConfigurationManager.AppSettings["EMailPassword"];
                    var smtpHost = ConfigurationManager.AppSettings["SMTPHost"];
                    var smtpPort = ConfigurationManager.AppSettings["SMTPPort"];
                    if (!string.IsNullOrWhiteSpace(emailAddresses))
                    {
                        string[] splitEmails = emailAddresses.Split(',');
                        foreach (var email in splitEmails)
                        {
                            try
                            {
                                if (!string.IsNullOrWhiteSpace(email))
                                {
                                    SmtpClient smtp = new SmtpClient();
                                    MailMessage Message = new MailMessage();
                                    Message.To.Add(email);
                                    Message.From = new MailAddress(emailFrom);
                                    Message.Subject = "Automated Message - Radio Apps is not Responding";
                                    Message.Body = "This email was sent automatically from the hourly radio apps check tool.<br /><br /> There is currently a negative response from the radio apps service.";
                                    Message.IsBodyHtml = true;

                                    smtp.Send(Message);
                                    Console.WriteLine("Sending Message..");
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("Failure to send message Exception: " + ex.Message);
                            }
                        }
                    }
                }
            }
        } 
        */ // old, doesnt restart app pools
        public static void TestServerResponseAndRestart(string appPoolName) // test if app is up, restart if not up, restarts once a day
        {
            if (!string.IsNullOrEmpty(appPoolName))
            {
                try
                {
                    // Set to always stop server to fire email to test if system is working
                    var DailyServerRestart = ConfigurationManager.AppSettings["DailyServerRestart"];
                    var ServerRestartHour = ConfigurationManager.AppSettings["ServerRestartHour"];

                    if (DailyServerRestart.ToLower().Trim() == "true")
                    {
                        bool isTestTIme = false;
                        if (DateTime.Now.Hour == int.Parse(ServerRestartHour))
                            isTestTIme = StopAppPool(appPoolName);
                    }

                    try
                    {
                        var httpRequestURL = ConfigurationManager.AppSettings["HTTPRequestURL"];
                        var CredentialsAuthType = ConfigurationManager.AppSettings["CredentialsAuthType"];
                        var NetworkCredentialsUserName = ConfigurationManager.AppSettings["NetworkCredentialsUserName"];
                        var NetworkCredentialsPassword = ConfigurationManager.AppSettings["NetworkCredentialsPassword"];

                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(httpRequestURL);
                        CredentialCache cc = new CredentialCache();
                        cc.Add(new Uri(httpRequestURL), CredentialsAuthType, new NetworkCredential(NetworkCredentialsUserName, NetworkCredentialsPassword));

                        request.Credentials = cc;
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                        if (response != null || response.StatusCode == HttpStatusCode.OK)
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.Write(HttpStatusCode.OK);
                            Console.ResetColor();
                            Console.WriteLine();

                            WriteToEventLog("Successfully recieved server response.");

                            var TestEmail = ConfigurationManager.AppSettings["TestEmail"];
                            if (TestEmail == "true")
                            {
                                if (ConfigurationManager.AppSettings["SendEmail"].ToLower().Trim() == "true")
                                {
                                    var emailAddresses = ConfigurationManager.AppSettings["EmailList"] ?? "daniel.barrett@infonote.com";
                                    var emailFrom = ConfigurationManager.AppSettings["EmailFrom"];
                                    if (!string.IsNullOrWhiteSpace(emailAddresses))
                                    {
                                        string[] splitEmails = emailAddresses.Split(',');
                                        foreach (var email in splitEmails)
                                        {
                                            try
                                            {
                                                if (!string.IsNullOrWhiteSpace(email))
                                                {
                                                    var AppName = ConfigurationManager.AppSettings["AppName"];
                                                    SmtpClient smtp = new SmtpClient();
                                                    MailMessage Message = new MailMessage();
                                                    Message.To.Add(email);
                                                    Message.From = new MailAddress(emailFrom);
                                                    Message.Subject = "Automated Message - " + AppName + " Mail Client is working";
                                                    Message.Body = "This email is testing that the mail client is working.<br /> <br /> To disable this message, change the TestEmail in the app.config to false.";
                                                    Message.IsBodyHtml = true;

                                                    smtp.Send(Message);
                                                    Console.WriteLine("Sending Message..");
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Console.WriteLine("Failure to send message Exception: " + ex.Message);
                                                WriteErrorToEventLog(ex.ToString());

                                            }
                                        }
                                    }
                                }
                            }
                            response.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        WriteErrorToEventLog(ex.ToString());

                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("Not " + HttpStatusCode.OK);
                        Console.ResetColor();
                        Console.WriteLine();

                        string appPoolRestartResult = "";

                        Console.WriteLine("Restarting App Pool..");
                        if (StartAppPool(appPoolName))
                        {
                            Console.WriteLine("App Pool has Restarted.");
                            appPoolRestartResult = "was successful.";
                        }
                        else
                        { 
                            Console.WriteLine("Failed to Restart App Pool.");
                            appPoolRestartResult = "failed.";
                        }
                        Console.WriteLine();

                        if (ConfigurationManager.AppSettings["SendEmail"] == "true")
                        {
                            var emailAddresses = ConfigurationManager.AppSettings["EmailList"] ?? "daniel.barrett@infonote.com";
                            var emailFrom = ConfigurationManager.AppSettings["EmailFrom"];
                            if (!string.IsNullOrWhiteSpace(emailAddresses))
                            {
                                string[] splitEmails = emailAddresses.Split(',');
                                foreach (var email in splitEmails)
                                {
                                    try
                                    {
                                        if (!string.IsNullOrWhiteSpace(email))
                                        {
                                            var AppName = ConfigurationManager.AppSettings["AppName"];
                                            SmtpClient smtp = new SmtpClient();
                                            MailMessage Message = new MailMessage();
                                            Message.To.Add(email);
                                            Message.From = new MailAddress(emailFrom);
                                            Message.Subject = AppName + " is not Responding";
                                            Message.Body = "This email was sent automatically from the server response check tool.<br /><br /> There was a negative response from the service.<br /><br /> The automated app pool restart reports that it " + appPoolRestartResult;
                                            Message.IsBodyHtml = true;

                                            smtp.Send(Message);
                                            Console.WriteLine("Sending Message..");
                                        }
                                    }
                                    catch (Exception ex2)
                                    {
                                        Console.WriteLine("Failure to send message Exception: " + ex2.Message);
                                        WriteErrorToEventLog(ex2.ToString());
                                    }
                                }
                            }
                        }
                    }
                        
                }
                catch (Exception ex)
                {
                    WriteErrorToEventLog(ex.ToString());
                }
            }
            else
            {
                Console.WriteLine(string.Format("Unable to test on app pool {1}", appPoolName));
                WriteErrorToEventLog(string.Format("Unable to test on app pool {1}", appPoolName));
            }
        }

        public static bool StopAppPool(string appPoolName)
        {
            bool rtn = true;
            using (ServerManager manager = new ServerManager())
            {
                ApplicationPool appPool = manager.ApplicationPools.FirstOrDefault(ap => ap.Name == appPoolName);

                if (appPool != null)
                {
                    try
                    {
                        if (appPool != null)
                        {
                            if (appPool.State == ObjectState.Unknown) throw new Exception("Object state is unknown.");

                            if (appPool.State == ObjectState.Stopped)
                                rtn = true;
                            else
                            {
                                if (appPool.State != ObjectState.Started)
                                {
                                    bool cancontinue = true;
                                    int count = 0;
                                    if (appPool.State == ObjectState.Starting)
                                    {
                                        while (cancontinue)
                                        {
                                            if (appPool.State == ObjectState.Started)
                                            {
                                                cancontinue = false;
                                            }
                                            else
                                            {
                                                count++;
                                                if (count > maxCount) throw new Exception("Trying to stop, but app pool still starting.");
                                                System.Threading.Thread.Sleep(1000);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        while (cancontinue)
                                        {
                                            if (appPool.State == ObjectState.Stopped)
                                            {
                                                return true;
                                            }
                                            else
                                            {
                                                count++;
                                                if (count > maxCount) throw new Exception("Trying to stop, but app pool still stopping before app pool stop command.");
                                                System.Threading.Thread.Sleep(1000);
                                            }
                                        }
                                    }

                                }
                                appPool.Stop();
                                if (appPool.State == ObjectState.Stopped)
                                    rtn = true;
                                else if (appPool.State == ObjectState.Stopping)
                                {
                                    bool cancontinue = true;
                                    int count = 0;
                                    while (cancontinue)
                                    {
                                        if (appPool.State == ObjectState.Stopped)
                                        {
                                            return true;
                                        }
                                        else
                                        {
                                            count++;
                                            if (count > maxCount) return false;
                                            System.Threading.Thread.Sleep(1000);
                                        }
                                    }
                                }
                                else throw new Exception("Trying to stop using app pool stop command but doing it's own thing");
                            }
                        }
                        else
                            rtn = false;
                    }
                    catch (Exception ex)
                    {
                        rtn = false;
                        WriteErrorToEventLog(ex.ToString());
                    }                    
                }
            }
            return rtn;
        }

        public static bool StartAppPool(string appPoolName)
        {
            bool rtn = false;
            using (ServerManager manager = new ServerManager())
            {
                ApplicationPool appPool = manager.ApplicationPools.FirstOrDefault(ap => ap.Name == appPoolName);

                if (appPool != null)
                {
                    try
                    {
                        if (appPool != null)
                        {
                            if (appPool.State == ObjectState.Unknown) throw new Exception("Object state is unknown.");

                            if (appPool.State == ObjectState.Started)
                                rtn = true;
                            else
                            {
                                if (appPool.State != ObjectState.Stopped)
                                {
                                    bool cancontinue = true;
                                    int count = 0;

                                    if (appPool.State == ObjectState.Stopping)
                                    {
                                        while (cancontinue)
                                        {
                                            if (appPool.State == ObjectState.Stopped)
                                            {
                                                cancontinue = false;
                                            }
                                            else
                                            {
                                                count++;
                                                if (count > maxCount) throw new Exception("Trying to start, but app pool still stopping.");
                                                System.Threading.Thread.Sleep(1000);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        while (cancontinue)
                                        {
                                            if (appPool.State == ObjectState.Started)
                                            {
                                                return true;
                                            }
                                            else
                                            {
                                                count++;
                                                if (count > maxCount) throw new Exception("Trying to start, but app pool still starting before app pool start command.");
                                                System.Threading.Thread.Sleep(1000);
                                            }
                                        }
                                    }
                                }
                                appPool.Start();
                                if (appPool.State == ObjectState.Started)
                                    rtn = true;
                                else if (appPool.State == ObjectState.Starting)
                                {
                                    bool cancontinue = true;
                                    int count = 0;
                                    while (cancontinue)
                                    {
                                        if (appPool.State == ObjectState.Started)
                                        {
                                            return true;
                                        }
                                        else
                                        {
                                            count++;
                                            if (count > maxCount) return false;
                                            System.Threading.Thread.Sleep(1000);
                                        }
                                    }
                                }
                                else throw new Exception("Trying to start using app pool start command but doing it's own thing");
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        rtn = false;
                        WriteErrorToEventLog(ex.ToString());
                    }
                }
            }
            return rtn;
        }

        public static void WriteErrorToEventLog(string message)
        {
            try
            {
                var addEventLog = ConfigurationManager.AppSettings["AddEventLog"];
                if (addEventLog.ToLower().Trim() == "true")
                {
                    var cs = ConfigurationManager.AppSettings["EventLogSource"];
                    EventLog elog = new EventLog();

                    if (!EventLog.SourceExists(cs))
                        EventLog.CreateEventSource(cs, "Application");

                    EventLog.WriteEntry(cs, message, EventLogEntryType.Error);
                }
            }
            catch (Exception ex)
            {
            }
            
        }
        public static void WriteToEventLog(string message)
        {
            try
            {
                var addEventLog = ConfigurationManager.AppSettings["AddEventLog"];
                if (addEventLog.ToLower().Trim() == "true")
                {
                    var cs = ConfigurationManager.AppSettings["EventLogSource"];
                    EventLog elog = new EventLog();

                    if (!EventLog.SourceExists(cs))
                        EventLog.CreateEventSource(cs, "Application");

                    EventLog.WriteEntry(cs, message, EventLogEntryType.Information);
                }
            }
            catch (Exception ex)
            {
            }
            
        }
    }
}
